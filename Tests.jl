module Tests


include("SquareLattices.jl")
using .SquareLattices

include("KochSquares.jl")
using .KochSquares
using BenchmarkTools

using SparseArrays
using KrylovKit
using ArnoldiMethod
using Arpack
using LinearAlgebra, LinearMaps

using LaTeXStrings
using CairoMakie


export fracgridtestplot, filltests, eigentest


CairoMakie.activate!(type="pdf", px_per_unit=1)
Makie.inline!(false)

function fracgridtestplot()
    f = Figure(resolution=(600, 600))
    ax = Axis(f[1, 1])

    max_level = 3
    frac_level = max_level - 1
    sl = SquareLattice(max_level, 1.0)
    koch = KochSquare(max_level, frac_level)

    for I in CartesianIndices(sl.positions)
        scatter!(ax, sl.positions[I], color=:black, markersize=2, marker=:rect, opacity=0.2)
    end
    scatter!(ax, sl.positions[sl.center], color=:red, markersize=5, marker=:rect)

    lines!(ax, [sl.positions[I] for I in koch.idxs], color=(:blue, 0.5), linewidth=2)

    display(f)
end


function filltests(max_level::Int64=3)
    frac_level = max_level
    # sl1 = SquareLattice(max_level, 1.0)
    # sl2 = SquareLattice(max_level, 1.0)
    # sl3 = SquareLattice(max_level, 1.0)
    # sl4 = SquareLattice(max_level, 1.0)
    koch = KochSquare(max_level, frac_level)

    offset = sum(4 .^ [l for l in 0:max_level-1]) + 1 + 1
    center = Int(4^max_level / 2) + offset

    bmp = fill(false, (2 * max_level + 1, 2 * max_level + 1))

    # # Set boundary on the square lattices
    # set_bdry!(sl1, koch.idxs)
    # set_bdry!(sl2, koch.idxs)
    # set_bdry!(sl3, koch.idxs)
    # set_bdry!(sl4, koch.idxs)

    # # Compare fill methods
    # display("Level " * string(max_level) * " - " * string(frac_level))
    # if max_level > 4
    #     display("Too many levels for comparison")
    #     display("Stack overflow for naive flood fill at level 5 or larger")
    # else
    #     display("Flood fill time")
    #     @time try
    #         flood_fill!(sl1)
    #     catch e
    #         display("Flood fill error")
    #         display(e)
    #     end
    # end


    # display("Image segmentation time")
    # if max_level > 5
    #     display("Too many levels for comparison")
    #     display("Heap RAM overflow for image seed segmentation at level 6 or larger")
    # else
    #     @time try
    #         imgseg_fill!(sl2)
    #     catch e
    #         display("Image segmentation error")
    #         display(e)
    #     end
    # end

    # display("Felzenszwalb time")
    # @time try
    #     felzenszwalb_fill!(sl3)
    # catch e
    #     display("Felzenszwalb image segmentation error")
    #     display(e)
    # end

    # display("Span fill time")
    # @time try
    #     span_fill!(sl4)
    # catch e
    #     display("Span fill error")
    #     display(e)
    # end


    # # Compare results
    # display("Same results?")


    # @show sum([sl1.types[i].type != sl2.types[i].type for i in CartesianIndices(sl1.types)])
    # @show sum([sl2.types[i].type != sl3.types[i].type for i in CartesianIndices(sl2.types)])
    # @show sum([sl3.types[i].type != sl4.types[i].type for i in CartesianIndices(sl3.types)])
    # # f = Figure(resolution=(1200, 600))
    # ax1 = Axis(f[1, 1])
    # ax2 = Axis(f[1, 2])
    # ax3 = Axis(f[1, 3])

    # plottable1 = [sl1.types[i].type == Internal for i in CartesianIndices(sl1.types)]
    # plottable2 = [sl2.types[i].type == Internal for i in CartesianIndices(sl2.types)]
    # bdry = [sl1.types[i].type == Boundary for i in CartesianIndices(sl1.types)]
    # heatmap!(ax1, plottable1, colormap=:grays)
    # heatmap!(ax2, plottable2, colormap=:grays)
    # heatmap!(ax3, bdry, colormap=:grays)

    # display(f)

end

function eigentest(max_level::Int64=3)::Nothing
    @time begin # Check preallocations before actual solving
        # Set initial conditions
        L = 1.0

        frac_level = max_level - 1
        sl = SquareLattice(max_level, L)
        koch = KochSquare(max_level, frac_level)

        # Set boundary on the square lattice
        set_bdry!(sl, koch.idxs)
        imgseg_fill!(sl)

        # Interior indices in a list
        intr = [I for I in CartesianIndices(sl.types) if sl.types[I].type == Internal]
        # bdry = [I for I in CartesianIndices(sl.types) if sl.types[I].type == Boundary]

        # Since the boundary and exterior is zero and constant, we only have to account for interior positions in the Laplacian
        # The elements in A must be defined from the neighbours of the interior points
        # This results in a sparse matrix

        # Sort by number of boundary neighbours
        # Near many boundary points = first, only interior neighbours = last
        # intr_sorted = sort(intr, by=(I -> count([sl.types[I].neighbours[i].type == Boundary for i in eachindex(sl.types[I].neighbours)])))

        # Define the laplacian
        N = size(sl.types, 1)
        N2 = N^2
        A = spzeros(N2, N2)

        for I in intr
            i, j = Tuple(I)

            ia = i + 1
            ib = i - 1
            ja = j + 1
            jb = j - 1

            A[(j-1)*N+i, (j-1)*N+i] = 4.0

            if sl.types[ia, j].type == Internal
                A[(j-1)*N+i, (j-1)*N+ia] = -1
            end

            if sl.types[ib, j].type == Internal
                A[(j-1)*N+i, (j-1)*N+ib] = -1
            end

            if sl.types[i, ja].type == Internal
                A[(j-1)*N+i, (ja-1)*N+i] = -1
            end

            if sl.types[i, jb].type == Internal
                A[(j-1)*N+i, (jb-1)*N+i] = -1
            end
        end
    end # timing and allocation measure
    display(A)

    # # Is A symmetric?
    # display("Is A symmetric? ")
    # display(issymmetric(A))
    base = 0.02

    v0 = zeros(N, N)
    v0[intr] = [rand() for i in 1:length(intr)]
    # v0[intr] .= base
    v0 = reduce(vcat, v0)
    display("Running eigensolvers ")

    nev = 20
    tol = 1e-6
    which = :SM
    check = 1
    maxiter = 300
    ritzvec = false

    display("Arpack")
    @time eigs1 = Arpack.eigs(A; v0=v0, nev=nev, tol=tol, which=which,
        check=check, maxiter=maxiter, ritzvec=ritzvec) # test Arpack

    display("Arpack with shift-and-invert")
    sigma = 0.0 + tol
    which = :LM
    @time eigs11 = Arpack.eigs(A; v0=v0, nev=nev, tol=tol, which=which,
        check=check, maxiter=maxiter, ritzvec=ritzvec, sigma=sigma) # test Arpack shift and invert
    # display("ArnoldiMethod")
    # @time begin
    #     decomp, arnhist = partialschur(A; nev=nev, tol=1e-10, which=SR(), restarts=500) # test ArnoldiMethod
    #     eigs2, X1 = partialeigen(decomp)
    # end
    # display(arnhist)

    # display("KrylovKit")
    # @time eigs3 = eigsolve(A, v0, nev, :SR; tol=1e-6, maxiter=300) # test krylowkit eigensolver

    # Y1 = Matrix{Float64}(undef, N2, size(eigs3[2], 1))
    # for i in 1:nev
    #     Y1[:, i] = eigs3[2][i]
    # end

    # @show size(eigs1[2]), size(eigs1[1])
    # @show size(eigs2), size(X1)
    # @show size(eigs3[2]), size(Y1)
    # println()
    # display("Removing noise λ < 1e-8")
    # efilter = (e -> (e > 1e-8) ? e : NaN)
    # e1 = eigs1[1] .|> efilter
    # e2 = eigs2 .|> efilter
    # e3 = eigs3[1] .|> efilter
    display("Eigenvalues comparison")

    # show(IOContext(stdout, :limit => false), MIME"text/plain"(), hcat(eigs1[1], eigs2, eigs3[1]))
    show(IOContext(stdout, :limit => false), MIME"text/plain"(), hcat(eigs1[1], eigs11[1]))
    println()
    @show norm(A * eigs1[2] - eigs1[2] * Diagonal(eigs1[1]))
    @show norm(A * eigs11[2] - eigs11[2] * Diagonal(eigs11[1]))
    # @show norm(A * X1 - X1 * Diagonal(eigs2))
    # @show norm(A * Y1 - Y1 * Diagonal(eigs3[1]))

    # f = Figure(resolution=(1000, 1000))
    # ax = Axis(f[1, 1],
    #     xlabel=L"$#$ Eigenvalue",
    #     ylabel=L"$\omega^2 / v^2$")
    # scatter!(ax, e1, color=(:red, 0.5), label="Arpack", marker='✈', markersize=20)
    # scatter!(ax, e2, color=(:blue, 0.5), label="ArnoldiMethod", marker=:rect, markersize=20)
    # scatter!(ax, e3, color=(:green, 0.5), label="KrylovKit", marker=:cross, markersize=20)

    # axislegend(ax)
    # display(f)
    # save("eigenmethods_noise_init4.pdf", f)

    # Test shift and invert methods
    # Factorizes A and builds a linear map that applies inv(A) to a vector.
    # function construct_linear_map(A)
    #     F = lu(A)
    #     LinearMap{eltype(A)}((y, x) -> ldiv!(y, F, x), size(A, 1), ismutating=true)
    # end

    # display("Shift and invert method for Arnoldi")
    # # Target the largest eigenvalues of the inverted problem
    # decomp, arnhist = partialschur(construct_linear_map(A), nev=20, tol=1e-6, restarts=300, which=LM())
    # λs_inv, X2 = partialeigen(decomp)
    # display(arnhist)

    # # Eigenvalues have to be inverted to find the smallest eigenvalues of the non-inverted problem.
    # λs = 1 ./ λs_inv

    # @show λs

    # @show norm(A * X2 - X2 * Diagonal(λs))

    # f = Figure(resolution=(1000, 1000))
    # ax = Axis(f[1, 1],
    #     xlabel=L"$#$ Eigenvalue",
    #     ylabel=L"$\omega^2 / v^2$")
    # scatter!(ax, λs, color=(:red, 0.5), label="ArnoldiMethod", marker=:rect, markersize=20)
    # scatter!(ax, e2, color=(:blue, 0.5), label="ArnoldiMethod with shift-and-invert", marker=:cross, markersize=20)

    # axislegend(ax)

    # display(f)
    # save("eigenmethods_shift_and_invert.pdf", f)

    return nothing
end


end # module Tests