


# include("SquareLattices.jl")
# using .SquareLattices
include("BoolMapTools.jl")
using SparseArrays
using Arpack
using LinearAlgebra


# struct Params
#     boundary::Vector{CartesianIndex{2}}
#     internals::Vector{CartesianIndex{2}}
#     δ::Float64

#     function Params(boundary::Vector{CartesianIndex{2}}, internals::Vector{CartesianIndex{2}}, Δx::Float64)
#         new(boundary, internals, Δx)
#     end
# end

function eigensolve(max_level::Int64=3)

    display("Lattice construction")
    @time begin
        # Set initial conditions
        L = 1.0
        frac_level = max_level
        bmp = make_bitmap(max_level)
        koch = KochSquare(max_level, frac_level)

        # Set boundary on the square lattice
        span_fill!(bmp, koch.idxs)

        intr = [I for I in CartesianIndices(bmp) if bmp[I]]
    end # time

    display(" -∇² construction")
    @time begin
        # Define the laplacian
        N = size(bmp, 1)
        N2 = N^2

        # Making diagonals
        mdiag = zeros(N2) # central node difference

        xa1diag = zeros(N2 - 1) # right node difference
        xa2diag = zeros(1) # wrap around right

        xb1diag = zeros(N2 - 1) # left node difference
        xb2diag = zeros(1) # wrap around left


        ya1diag = zeros(N2 - N) # above node difference
        ya2diag = zeros(N) # wrap around above

        yb1diag = zeros(N2 - N) # below node difference
        yb2diag = zeros(N) # wrap around below

        # @inbounds @simd
        for I in intr
            J = (I[2] - 1) * N + I[1]
            mdiag[J] = 4.0
        end
        # @inbounds @simd
        for I in intr
            J = (I[2] - 1) * N + I[1] + 1
            if bmp[J]
                if J <= length(xa1diag)
                    xa1diag[J] = -1.0
                else
                    xa2diag[J-length(xa1diag)] = -1.0
                end
            end
        end
        # @inbounds @simd
        for I in intr
            J = (I[2] - 1) * N + I[1] - 1
            if bmp[J]
                if J >= 1
                    xb1diag[J] = -1.0
                else
                    xb2diag[J+length(xb1diag)] = -1.0
                end
            end
        end
        # @inbounds @simd
        for I in intr
            J = (I[2] - 1) * N + I[1] + N
            if bmp[J]
                # Must have wrapping behaviour here
                if J <= length(ya1diag)
                    ya1diag[J] = -1.0
                else
                    ya2diag[J-length(ya1diag)] = -1.0
                end
            end
        end
        for I in intr
            J = (I[2] - 1) * N + I[1] - N
            if bmp[J]
                # Must have wrapping behaviour here
                if J >= 1
                    yb1diag[J] = -1.0
                else
                    yb2diag[J+length(yb1diag)] = -1.0
                end
            end
        end
        # Construct band diagonal matrix
        A = spdiagm(0 => mdiag, 1 => xa1diag, -1 => xb1diag, N => ya1diag, -N => yb1diag, -N2 + N => ya2diag, N2 - N => yb2diag)
    end # time
    # display(A)

    v0 = zeros(N, N)
    v0[intr] = [0.1 for i in 1:length(intr)]
    # v0[intr] .= base
    v0 = reduce(vcat, v0)

    # display(v0)

    display("Running eigensolver")

    nev = 10
    tol = 1e-6
    check = 1
    maxiter = 300
    ritzvec = true
    sigma = 0.0 + eps()
    which = :LR

    # sigma = nothing
    # which = :SR
    @time eigs = Arpack.eigs(A; v0=v0, nev=nev, tol=tol, which=which,
        check=check, maxiter=maxiter, ritzvec=ritzvec, sigma=sigma) # test Arpack shift and invert


    @show norm(A * eigs[2] - eigs[2] * Diagonal(eigs[1]))
    return eigs
end