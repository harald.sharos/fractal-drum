using Revise

# include("Tests.jl")
# using .Tests
include("FracEigenSolver.jl")


@enum ProgramType begin
    Eigen
    EigenTest
    FracGridTests
    FillTests
end

function main(pt::ProgramType)
    if pt == FillTests
        # Fill algorithm tests
        filltests(3)
        return
    elseif pt == FracGridTests
        # Fractal grid tests
        fracgridtestplot()
        return
    elseif pt == EigenTest
        eigentest(4)
        return
    elseif pt == Eigen
        res = eigensolve(4)
        return real.(res[1])
    end
end

res = main(Eigen)