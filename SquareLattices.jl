module SquareLattices

export SquareLattice, set_bdry!, flood_fill!, span_fill!, imgseg_fill!, felzenszwalb_fill!, NodePoint, NodeType, None, Boundary, Internal

using StaticArrays
using DataStructures
using ImageSegmentation: seeded_region_growing, felzenszwalb, labels_map


@enum NodeType begin
    None
    Boundary
    Internal
end

mutable struct NodePoint <: FieldVector{2,Int64}
    const i::Int64
    const j::Int64
    type::NodeType
    neighbours::Vector{NodePoint}

    function NodePoint(; i::Int64, j::Int64, type::NodeType=None, neighbours::Vector{NodePoint}=Vector{NodePoint}())
        new(i, j, type, neighbours)
    end
    function NodePoint(i::Int64, j::Int64)
        new(i, j, None, Vector{NodePoint}())
    end
end

struct SquareLattice
    positions::Matrix{SVector{2,Float64}}
    types::Matrix{NodePoint}
    max_level::Int64
    L::Float64
    center::CartesianIndex{2}
    δ::Float64

    function SquareLattice(max_level::Int64, L::Float64)

        offset = sum(4 .^ [l for l in 0:max_level-1]) + 1 + 1
        center = Int(4^max_level / 2) + offset

        δ = L / 4^max_level
        is = 0:2*(center-1)
        js = 0:2*(center-1)
        xs = [δ * i for i in is]
        ys = [δ * j for j in js]

        positions = Matrix{SVector{2,Float64}}(undef, length(is), length(js))
        for i in is
            for j in js
                positions[i+1, j+1] = SVector{2,Float64}(xs[i+1], ys[j+1])
            end
        end

        types = [NodePoint(i, j) for i in 1:length(is), j in 1:length(js)]

        for I in CartesianIndices(types)
            i, j = Tuple(I)
            if i > 1
                push!((types[i, j]).neighbours, types[i-1, j])
            end
            if i < size(types, 1)
                push!((types[i, j]).neighbours, types[i+1, j])
            end
            if j > 1
                push!((types[i, j]).neighbours, types[i, j-1])
            end
            if j < size(types, 2)
                push!((types[i, j]).neighbours, types[i, j+1])
            end
        end
        new(positions, types, max_level, L, CartesianIndex{2}(center, center), δ)
    end
end

function set_bdry!(sl::SquareLattice, idxs::Vector{CartesianIndex{2}})::Nothing
    for n in sl.types[idxs]
        n.type = Boundary
    end
end

function rec_flood!(np::NodePoint)::Nothing
    # Recursively fill the interior
    if np.type != None
        return
    end
    np.type = Internal
    for m in np.neighbours
        rec_flood!(m)
    end
end

function flood_fill!(sl::SquareLattice)::Nothing
    sl.types[sl.center].type = Internal
    rec_flood!(sl.types[sl.center])
    nothing
end

function span_fill!(sl::SquareLattice)::Nothing
    # Fill the interior by spanning the lattice
    bmp = [n.type == Boundary for n in sl.types]
    s = Stack{Tuple{Int64,Int64,Int64,Int64}}()
    # (x1,x2,y,dy)
    x = deepcopy(sl.center[1])
    y = deepcopy(sl.center[2])
    push!(s, (x, x, y, 1))
    push!(s, (x, x, y - 1, -1))

    while !isempty(s)
        x1, x2, y, dy = pop!(s)
        x = x1
        if !bmp[x, y]
            while !bmp[x-1, y]
                bmp[x-1, y] = true
                x -= 1
            end
        end
        if x < x1
            push!(s, (x, x1 - 1, y - dy, -dy))
        end
        while x1 <= x2
            while !bmp[x1, y]
                bmp[x1, y] = true
                x1 += 1
                push!(s, (x, x1 - 1, y + dy, dy))
                if x1 - 1 > x2
                    push!(s, (x2 + 1, x1 - 1, y - dy, -dy))
                end
            end
            x1 += 1
            while x1 < x2 && bmp[x1, y]
                x1 += 1
            end
        end
    end

    for I in CartesianIndices(bmp)
        type = sl.types[I].type
        type = (bmp[I] && type == None) ? Internal : type
    end
    nothing
end

function imgseg_fill!(sl::SquareLattice)::Nothing
    # Use ImageSegmentation.jl to fill the lattice

    img = [n.type == Boundary ? 1 : 0 for n in sl.types]

    seeds = [CartesianIndex(1, 1) => 1, sl.center => 2]
    segments = seeded_region_growing(img, seeds)

    for I in CartesianIndices(sl.types)
        if labels_map(segments)[I] == 2
            if sl.types[I].type == None
                sl.types[I].type = Internal
            end
        end
    end

    nothing

end

function felzenszwalb_fill!(sl::SquareLattice)::Nothing
    # Use ImageSegmentation.jl to fill the lattice

    img = [n.type == Boundary ? 1 : 0 for n in sl.types]

    segments = felzenszwalb(img, 300, 100)

    interior_lbl = labels_map(segments)[sl.center]

    for I in CartesianIndices(sl.types)
        if labels_map(segments)[I] == interior_lbl
            if sl.types[I].type == None
                sl.types[I].type = Internal
            end
        end
    end

    nothing

end

function fast_fill!(sl::SquareLattice)::Nothing
    # Use ImageSegmentation.jl to fill the lattice

    img = [n.type == Boundary ? 1 : 0 for n in sl.types]

    fast_scanning!(img, 300, 100)

    interior_lbl = labels_map(segments)[sl.center]

    for I in CartesianIndices(sl.types)
        if labels_map(segments)[I] == interior_lbl
            if sl.types[I].type == None
                sl.types[I].type = Internal
            end
        end
    end

    nothing

end



end # module