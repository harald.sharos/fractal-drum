include("KochSquares.jl")
using DataStructures

function make_bitmap(max_level::Int64)::BitMatrix
    offset = sum(4 .^ [l for l in 0:max_level-1]) + 1 + 1
    center = Int(4^max_level / 2) + offset

    is = 0:2*(center-1)
    js = 0:2*(center-1)

    return falses(length(is), length(js))
end

function rec_flood!(bmp::BitMatrix, x::Int64, y::Int64)::Nothing
    # Recursively fill the interior
    if bmp[x, y]
        return
    end

    bmp[x, y] = true

    rec_flood!(bmp, x - 1, y)
    rec_flood!(bmp, x + 1, y)
    rec_flood!(bmp, x, y - 1)
    rec_flood!(bmp, x, y + 1)

    return nothing
end


function flood_fill!(bmp::BitMatrix, bdry::Vector{CartesianIndex{2}})::Nothing
    N = size(bmp, 1)
    MID = round(Int, N / 2)
    bmp[bdry] .= true
    rec_flood!(bmp, MID, MID)
    nothing
end

function span_fill!(bmp::BitMatrix, bdry::Vector{CartesianIndex{2}})::Nothing
    # Fill the interior by spanning the lattice
    bmp[bdry] .= true

    # (x1,x2,y,dy)
    s = Stack{Tuple{Int64,Int64,Int64,Int64}}()

    N = size(bmp, 1)
    x = round(Int, N / 2)
    y = round(Int, N / 2)
    push!(s, (x, x, y, 1))
    push!(s, (x, x, y - 1, -1))

    while !isempty(s)
        x1, x2, y, dy = pop!(s)
        x = x1
        if !bmp[x, y]
            while !bmp[x-1, y]
                bmp[x-1, y] = true
                x -= 1
            end
        end
        if x < x1
            push!(s, (x, x1 - 1, y - dy, -dy))
        end
        while x1 <= x2
            while !bmp[x1, y]
                bmp[x1, y] = true
                x1 += 1
                push!(s, (x, x1 - 1, y + dy, dy))
                if x1 - 1 > x2
                    push!(s, (x2 + 1, x1 - 1, y - dy, -dy))
                end
            end
            x1 += 1
            while x1 < x2 && bmp[x1, y]
                x1 += 1
            end
            x = x1
        end
    end
    bmp[bdry] .= false
    reverse!(bmp, dims=1)
    nothing
end