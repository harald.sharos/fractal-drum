using StaticArrays

struct KochSquare
    """
    Holds list of indices of a fractal square lattice.
    """
    idxs::Vector{CartesianIndex{2}}
    scaled_level::Int64
    frac_level::Int64

    function KochSquare(scaled_level::Int64, frac_level::Int64)
        idxs = koch_square(scaled_level, frac_level)

        new(idxs, scaled_level, frac_level)
    end
end

#=
Pattern:
    1. Split line into 8 lines
    2. Rotate lines
    3. Move lines
    4. Repeat
Rotation pattern:
    1. Rotate 90 degrees
    2. Rotate 270 degrees
    3. Rotate 0 degrees
    4. Repeat

For the quadratic koch we get 4*8^ℓ points, which can be preallocated from knowing ℓ
=#

const pattern = [0, 1, 0, 3, 3, 0, 1, 0]

# Jeez louise, group theory or something idk
function idxs_recursion!(subs::Vector{Int64}, ℓ::Int64=1)::Vector{Int64}
    if ℓ == 0
        return subs
    else
        subsize = 8^(ℓ - 1)

        for i in 1:8
            subs[(i-1)*subsize+1:i*subsize] .= pattern[i] .+ idxs_recursion!(subs[(i-1)*subsize+1:i*subsize], ℓ - 1)
        end

        # subs[0subsize+1:1subsize] = 0 .+ idxs_recursion!(subs[0subsize+1:1subsize], ℓ-1)
        # subs[1subsize+1:2subsize] = 1 .+ idxs_recursion!(subs[1subsize+1:2subsize], ℓ-1)
        # subs[2subsize+1:3subsize] = 0 .+ idxs_recursion!(subs[2subsize+1:3subsize], ℓ-1)
        # subs[3subsize+1:4subsize] = 3 .+ idxs_recursion!(subs[3subsize+1:4subsize], ℓ-1)
        # subs[4subsize+1:5subsize] = 3 .+ idxs_recursion!(subs[4subsize+1:5subsize], ℓ-1)
        # subs[5subsize+1:6subsize] = 0 .+ idxs_recursion!(subs[5subsize+1:6subsize], ℓ-1)
        # subs[6subsize+1:7subsize] = 1 .+ idxs_recursion!(subs[6subsize+1:7subsize], ℓ-1)
        # subs[7subsize+1:8subsize] = 0 .+ idxs_recursion!(subs[7subsize+1:8subsize], ℓ-1)
        return subs
    end
end

function find_rots(levels::Int64)::Vector{Int64}
    rots = idxs_recursion!(zeros(Int64, 8^levels), levels)
    return rots
end

function koch_square(max_level::Int64, frac_level::Int64)::Vector{CartesianIndex{2}}

    """
    Returns a vector of 2D cartesian indices
        containing the respective i,j coordinates from 1:8^frac_levels+1
        for the Koch square of level `frac_levels`.
        Scales up the indices to correspond to the level `max_level`.
    """
    rots = find_rots(frac_level) .+ 1

    Δ = 4^(max_level - frac_level)
    rot0 = CartesianIndex{2}(1, 0)
    rot90 = CartesianIndex{2}(0, 1)
    rot180 = CartesianIndex{2}(-1, 0)
    rot270 = CartesianIndex{2}(0, -1)

    rot_list = [rot0, rot90, rot180, rot270]

    # Offset indices so they are positive
    offset = sum(4 .^ [l for l in 0:max_level-1]) + 1 + 1
    side1 = fill(CartesianIndex{2}(offset, offset), Δ * 8^frac_level + 1)
    side2 = Vector{CartesianIndex{2}}(undef, length(side1))
    side3 = Vector{CartesianIndex{2}}(undef, length(side1))
    side4 = Vector{CartesianIndex{2}}(undef, length(side1))

    # Fractal for side 1
    for i in 1:8^frac_level
        rot = mod1(rots[i] + 0, 4)
        for j in 1:Δ
            idx = (i - 1) * Δ + j + 1
            side1[idx] = side1[idx-1] + rot_list[rot]
        end
    end

    # Fractal for side 2 starts at end of side 1
    side2[begin] = side1[end]
    for i in 1:8^frac_level
        rot = mod1(rots[i] + 1, 4)
        for j in 1:Δ
            idx = (i - 1) * Δ + j + 1
            side2[idx] = side2[idx-1] + rot_list[rot]
        end
    end

    # 3
    side3[begin] = side2[end]
    for i in 1:8^frac_level
        rot = mod1(rots[i] + 2, 4)
        for j in 1:Δ
            idx = (i - 1) * Δ + j + 1
            side3[idx] = side3[idx-1] + rot_list[rot]
        end
    end

    # 4
    side4[begin] = side3[end]
    for i in 1:8^frac_level
        rot = mod1(rots[i] + 3, 4)
        for j in 1:Δ
            idx = (i - 1) * Δ + j + 1
            side4[idx] = side4[idx-1] + rot_list[rot]
        end
    end

    # combine 4 sides into one vector
    points = vcat(side1, side2, side3, side4)

    return points
end