# Fractal drum eigenmode calculations

This project is one of three in the subject "TFY4235 - Computational Physics" at NTNU. The subject concerns numerical methods in physics and is based on three projects with a one week final numerical exam.

This project asks the question "What sound does a fractal drum make?". This is implemented by creating a Koch fractal, modelling the Helmholtz equation on it with Dirichlet boundary conditions. Then the discretized linear operator can be solved for the most significant eigenmodes. Some resulting images are found under `img`.

The project is written in Julia, using Arpack, StaticArrays and DataStructures. The Koch fractal is implemented in `KochSquares.jl` where one can choose between the recursive and iterative approach. The implementation exploits the square symmetry of the Koch fractal to reduce the fractal problem to an integer indexing problem. The resulting grid can then be solved by the implementation found in `FracEigenSolver.jl`. `BoolMapTools.jl` provides different approaches for boolean masking of the interior.